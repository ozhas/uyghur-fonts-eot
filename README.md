# Uyghur-Fonts-eot

#### 介绍
收集常用维吾尔文字EOT文件及使用方法！

#### 软件架构
1. UKIJ Ekran 字体
2. Alpida Unicode System 字体
3. Ukij Tuz Tom
4. Alp Ekran 字体


#### 安装教程

1. 下载git代码包：

   ```bash
   git clone https://gitee.com/qeyser/uyghur-fonts-eot.git
   ```

2. html文件 head 区域引入相应的 style.css 文件

   ```html
   <!DOCTYPE html>
   <html dir="rtl">
       <head>
           <title>Web page</title>
           <meta charset="utf-8">
           <meta name="viewport" content="width=device-width, initial-scale=1">
           <link rel="stylesheet" href="fonts/style.css" />
       </head>
       <body>
           
       </body>
   </html>
   ```


#### 特技

1.  有意见和建议请联系：125790757
2.  请添加公众号 ：Qeyser技术圈

![输入图片说明](https://images.gitee.com/uploads/images/2021/0210/220649_35f6a476_11825.jpeg "qrcode.jpg")